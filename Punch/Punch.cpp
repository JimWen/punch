
// Punch.cpp : 定义应用程序的类行为。
//

#include "stdafx.h"
#include "Punch.h"
#include "PunchDlg.h"

#define one	TEXT("0xb17c81da, 0xfdb4, 0x40e8, 0x8e, 0x2a, 0x8b, 0x37, 0x6c, 0x36, 0x5c, 0x15")


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CPunchApp

BEGIN_MESSAGE_MAP(CPunchApp, CWinAppEx)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CPunchApp 构造

CPunchApp::CPunchApp()
{
	// TODO: 在此处添加构造代码，
	// 将所有重要的初始化放置在 InitInstance 中
	m_OneHandle = NULL;
}


// 唯一的一个 CPunchApp 对象

CPunchApp theApp;


// CPunchApp 初始化

BOOL CPunchApp::InitInstance()
{
	//应用程序单实例化
	m_OneHandle = ::CreateMutex(NULL, FALSE, one);//handle为声明的HANDLE类型的全局变量  
	if(GetLastError()==ERROR_ALREADY_EXISTS)  
	{
		MessageBox(NULL, TEXT("应用程序已经在运行"), TEXT("警告"), MB_OK);  
		return FALSE;  
	}

	//获得当前可执行文件路径和配置文件路径
	TCHAR szBuffer[MAX_PATH] ;											//临时存储可执行文件全路径
	TCHAR szDrive[10] ;													//可执行文件盘符
	TCHAR szDir[256] ;													//可执行文件目录
	TCHAR szFilename[64] ;												//可执行文件文件名
	TCHAR szExt[10] ;													//可执行文件文件后缀

	GetModuleFileName(NULL, szBuffer, MAX_PATH) ;
	_tsplitpath(szBuffer, szDrive, szDir, szFilename, szExt) ;			//分割全路径
	
	m_csExeFileName = szBuffer;
	m_csConfigFileName.Format(TEXT("%s%sConfig.ini"), szDrive, szDir) ;	//得到配置文件路径

	//判断配置文件是否存在
	WIN32_FIND_DATA fd;
	if (INVALID_HANDLE_VALUE == FindFirstFile(m_csConfigFileName.GetBuffer(MAX_PATH), &fd))
	{
		MessageBox(NULL, TEXT("配置文件丢失,程序无法启动"), TEXT("错误"), MB_OK | MB_ICONERROR);
		return FALSE;
	}

	// 如果一个运行在 Windows XP 上的应用程序清单指定要
	// 使用 ComCtl32.dll 版本 6 或更高版本来启用可视化方式，
	//则需要 InitCommonControlsEx()。否则，将无法创建窗口。
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// 将它设置为包括所有要在应用程序中使用的
	// 公共控件类。
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinAppEx::InitInstance();

	//采用非模态对话框便于程序一启动就隐藏对话框
	m_pMainWnd = new CPunchDlg;
	((CPunchDlg *)m_pMainWnd)->Create(IDD_SETTING, NULL);

	return TRUE;
}

int CPunchApp::ExitInstance()
{
	// 收尾工作
	if (NULL != m_OneHandle)
	{
		CloseHandle(m_OneHandle);
	}

	return CWinAppEx::ExitInstance();
}
