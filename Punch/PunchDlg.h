
// PunchDlg.h : 头文件
//

#pragma once
#include "afxwin.h"
#include "afxdtctl.h"
#include "Ini.h"
#include "afxcmn.h"


// CPunchDlg 对话框
class CPunchDlg : public CDialog
{
// 构造
public:
	BOOL m_bHotKey_Shift;
	BOOL m_bHotKey_Ctrl;
	BOOL m_bHotKey_Alt;
	INT m_nHotkey_Key;
	UINT m_nSetModifier;
	WORD m_wShowModifier;
	WORD m_atom;
	BOOL m_bIsToTray;
	CPunchDlg(CWnd* pParent = NULL);	// 标准构造函数

	//托盘图标
	BOOL ToTray();
	BOOL DeleteTray();
	LRESULT OnNotifyIcon(WPARAM wParam, LPARAM lParam);

	//注册表
	BOOL SetAutoRun();
	BOOL CancelAutoRun();

private:
	CTime m_timeMornAct;
	CTime m_timeNoonAct;
	CTime m_timeAfterAct;
	CTime m_timeNightAct;
	HICON m_hIcon;
	NOTIFYICONDATA m_nid;
	CIni m_configIni;

// 对话框数据
	enum { IDD = IDD_SETTING };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持

// 实现
protected:
	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnDestroy();
	afx_msg void OnSetting();
	afx_msg void OnHelp();
	afx_msg void OnAbout();
	afx_msg void OnExit();
	BOOL m_bMornEnable;
	CTime m_timeMorn;
	UINT m_nMornSpace;
	BOOL m_bNoonEnable;
	CTime m_timeNoon;
	UINT m_nNoonSpace;
	BOOL m_bAfterEnable;
	CTime m_timeAfter;
	UINT m_nAfterSpace;
	BOOL m_bNightEnable;
	CTime m_timeNight;
	UINT m_nNightSpace;
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnNcPaint();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	BOOL m_bAutoRun;
	BOOL m_bStartNotify;
	BOOL m_bEndNotify;
	afx_msg void OnBnClickedStartNotify();
	afx_msg void OnBnClickedAutorun();
	CHotKeyCtrl m_HotKeyCtrl;
	afx_msg void OnHotKey(UINT nHotKeyId, UINT nKey1, UINT nKey2);
	BOOL m_bHide;
	afx_msg BOOL OnQueryEndSession();
};
