// Ini.h: interface for the CIni class.
//
//////////////////////////////////////////////////////////////////////
#pragma once

class CIni  
{
public:
	CIni();
	CIni(LPCTSTR szFileName);
	virtual ~CIni();
	BOOL SetSection(LPCTSTR lpAppName, LPCTSTR lpString);
	BOOL SetString(LPCTSTR lpAppName, LPCTSTR lpKeyName, LPCTSTR lpString);
	BOOL SetStruct(LPCTSTR lpszSection, LPCTSTR lpszKey, LPVOID lpStruct, UINT uSizeStruct);
	BOOL SetInt(LPCTSTR lpAppName, LPCTSTR lpKeyName, const INT nValue);
	BOOL SetDouble(LPCTSTR lpAppName, LPCTSTR lpKeyName, const DOUBLE dbValue);
	CString GetSection(LPCTSTR lpAppName);
	CString GetSectionNames();
	CString GetString(LPCTSTR lpAppName, LPCTSTR lpKeyName, LPCTSTR lpDefault);
	BOOL GetStruct(LPCTSTR lpszSection, LPCTSTR lpszKey, LPVOID lpStruct, UINT uSizeStruct);
	INT GetInt(LPCTSTR lpAppName, LPCTSTR lpKeyName, const INT nDefault);
	DOUBLE GetDouble(LPCTSTR lpAppName, LPCTSTR lpKeyName, const DOUBLE dbDefault);

private:
	CString m_csFileName;
};